//
// Created by anton on 2021-09-11.
//

#include "../include/ProducerConsumer.h"

ProducerConsumer::ProducerConsumer(int pElements, int pProducerTh, int pConsumerTh) :
        elements(pElements), producerThreads(pProducerTh), consumerThreads(pConsumerTh) {}

void ProducerConsumer::producer(std::shared_ptr<BlockingQueue> blockingQueue, int startInt, int endInt) {
    for (int i = startInt; i < endInt; i++) {
        blockingQueue->put(i);
    }
}

std::shared_ptr<std::deque<int>> ProducerConsumer::consumer(std::shared_ptr<BlockingQueue> blockingQueue,
                                                            int numsPerConsumerThread) {
    std::cout << "\nConsumer thread started";
    std::shared_ptr<std::deque<int>> results = std::make_shared<std::deque<int>>();
    for (int i = 0; i < numsPerConsumerThread; i++) {
        results->emplace_front(blockingQueue->take());
    }
    return results;
}

void ProducerConsumer::run() {
    std::cout << "Handling " << elements << " values with " << producerThreads << " producers and " << consumerThreads
              << " consumers\n";
    int blockSize = elements / producerThreads;
    int lastBlockSize = blockSize + (elements % producerThreads);
    auto blockingQueue = std::make_shared<BlockingQueue>(BUFFER_SIZE);

    std::vector<std::thread> producers;
    int startInt = 0;
    int endInt = 0;
    for (int i = 0; i < producerThreads - 1; i++) {
        startInt = i * blockSize;
        endInt = startInt + blockSize;
        std::thread producerThread(&ProducerConsumer::producer, ProducerConsumer(), blockingQueue, startInt, endInt);
        std::cout << "Started a producer for " << blockSize << " values Interval [" << startInt << ".." << endInt
                  << "]\n";
        producers.push_back(std::move(producerThread));
    }
    startInt += blockSize;
    std::thread producerThread(&ProducerConsumer::producer, ProducerConsumer(), blockingQueue, startInt, elements);
    std::cout << "Started a producer for " << lastBlockSize << " values Interval [" << startInt << ".." << elements
              << "]\n";
    producers.push_back(std::move(producerThread));

    std::vector<std::future<std::shared_ptr<std::deque<int>>>> consumers;
    int consumptionBlockSize = elements / consumerThreads;
    int lastConsumptionBlockSize = consumptionBlockSize + (elements % consumerThreads);
    for (int i = 0; i < consumerThreads - 1; i++) {
        std::future<std::shared_ptr<std::deque<int>>>
                fu = std::async(std::launch::async, &ProducerConsumer::consumer, ProducerConsumer(), blockingQueue,
                                consumptionBlockSize);
        consumers.push_back(std::move(fu));
    }
    std::future<std::shared_ptr<std::deque<int>>>
            fu = std::async(std::launch::async, &ProducerConsumer::consumer, ProducerConsumer(), blockingQueue,
                            lastConsumptionBlockSize);
    consumers.push_back(std::move(fu));
    std::vector<std::deque<int>> results;
    for (auto &consumer : consumers) {
        std::cout << "\nWaiting ... ";
        consumer.wait();
        results.push_back(*consumer.get());
        std::cout << "\ngot a result";
    }

    long expectedSum = 0;
    for (int i = 0; i < elements; i++) {
        expectedSum += i;
    }
    long calculatedSum = 0;
    for (auto &result : results) {
        for (auto &resultCell : result) {
            calculatedSum += resultCell;
        }
    }

    std::cout << "\nExpected sum: " << expectedSum;
    std::cout << "\nCalculated sum: " << calculatedSum;

    for (auto &prod : producers) {
        prod.join();
        std::cout << "\nProducer thread joined";
    }
}