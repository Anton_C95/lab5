//
// Created by anton on 2021-09-10.
//

#include "../include/BlockingQueue.h"


BlockingQueue::BlockingQueue(int pBufferSize) :
        bufferSize(pBufferSize) {}

void BlockingQueue::put(int integer) {
    std::unique_lock<std::mutex> uniqueLock(guard);
    cvPut.wait(uniqueLock, [this]{ return buffer.size() != bufferSize; });
    buffer.push(integer);
    uniqueLock.unlock();
    cvTake.notify_one();
}

int BlockingQueue::take() {
    std::unique_lock<std::mutex> uniqueLock(guard);
    cvTake.wait(uniqueLock, [this]{ return !buffer.empty(); } );
    int tmp = buffer.front();
    buffer.pop();
    uniqueLock.unlock();
    cvPut.notify_one();
    return tmp;
}
