//
// Created by anton on 2021-09-13.
//

#include "../include/ProducerConsumer.h"

/**
 * The reason more threads can not better the performance of the the program is due to the bottleneck created by the
 * blocking queue.
 * */

bool validateInput(int& input) {
    bool valid = false;
    std::cin >> input;
    if (std::cin.good() || input > 0) {
        valid = true;
    } else {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "\nInvalid input, try again: ";
    }
    return valid;
}

int main() {
    int elements, producerThreads, consumerThreads;
    std::cout << "C++ ConsumerProducer blocking queue\n" << "Enter number of elements to pass through the queue: ";
    while (!validateInput(elements));
    std::cout << "\nEnter number of producers: ";
    while (!validateInput(producerThreads));
    std::cout << "\nEnter number of consumers: ";
    while (!validateInput(consumerThreads));
    ProducerConsumer* producerConsumer = new ProducerConsumer(elements, producerThreads, consumerThreads);
    producerConsumer->run();
}