//
// Created by anton on 2021-09-10.

// Producer and Consumers are methods
//
// Producer will not be async just normal threads that will produce output to the blockingQueue
// Consumer is a functor which will return a..? ask Gustav
// Consumersidan: varje Consumer ska hämta sitt antal tal från kön och sedan leverera dessa i
// en returnerad container, t.ex. en deque.
//
// ?????? För att underlätta minneshanteringen kan varje Consumer
// allokera en deque<int> dynamiskt till en shared_ptr<int> som sedan kan läsas av huvudtråden. ????

// Consumer-koden kan köras i async-trådar. När en async har startats kan motsvarande
// future sparas i en container för att senare användas för att hämta resultatet, i princip

// std::vector<future<deque<int>>> consumerFutures


#ifndef LAB5_BLOCKINGQUEUE_H
#define LAB5_BLOCKINGQUEUE_H

#include <iostream>
#include <queue>
#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>

class BlockingQueue {
private:
    int bufferSize{};
    mutable std::mutex guard;
    std::condition_variable cvPut;
    std::condition_variable cvTake;
public:
    std::queue<int> buffer{};
    explicit BlockingQueue(int pBufferSize);

    void put(int integer);
    int take();
};


#endif //LAB5_BLOCKINGQUEUE_H
