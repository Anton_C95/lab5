//
// Created by anton on 2021-09-11.
//

#ifndef LAB5_PRODUCERCONSUMER_H
#define LAB5_PRODUCERCONSUMER_H

#include "BlockingQueue.h"
#include <deque>
#include <memory>
#include <future>

class ProducerConsumer {
private:
    const int BUFFER_SIZE = 10;
    int elements;
    int producerThreads;
    int consumerThreads;
public:
    explicit ProducerConsumer(int pElements = 1000000, int pProducerTh = 6, int pConsumerTh = 6);

    void producer(std::shared_ptr<BlockingQueue> blockingQueue, int startInt, int endInt);

    std::shared_ptr<std::deque<int>> consumer(std::shared_ptr<BlockingQueue> blockingQueue, int numsPerConsumerThread);

    void run();
};

#endif //LAB5_PRODUCERCONSUMER_H
